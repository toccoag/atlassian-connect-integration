FROM eclipse-temurin:21-jre-jammy

RUN mkdir -p /usr/app/

COPY ./build/libs/atlassian-connect-integration-0.0.1-SNAPSHOT.jar /usr/app/

WORKDIR /usr/app
ENTRYPOINT ["java","-jar","atlassian-connect-integration-0.0.1-SNAPSHOT.jar", "-Dagentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=8000"]
EXPOSE 8080
