const issueTag = document.getElementsByName('issueKey')[0].getAttribute('content');
const tokenTag = document.getElementsByName('token')[0].getAttribute('content');

const host = window.location.origin;
const artifactsUrl = modelName => `${host}/jira/artifact/${modelName}`;
const selectedArtifactsUrl = `${host}/jira/artifact/issue/${issueTag}`;
const updateArtifactsUrl = `${host}/jira/artifact/issue/${issueTag}`;
const commitUrl = `${host}/jira/issue/${issueTag}/commit`;
const installationUrl = `${host}/jira/installation`;

const headers = {
  'Authorization': `JWT ${tokenTag}`,
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Headers': 'Origin, Content-Type, X-Auth-Token'
};

const getData = async(url) => {
  const res = await fetch(url, {headers});
  if (!res.ok) {
    throw Error(`Request failed with status ${res.status}`);
  }
  return res.json()
};

const putData = async (url, body) => {
  const res = await fetch(url, {
    method: 'PUT',
    body: body,
    headers: {...headers, 'Content-Type': 'application/json'}
  });
  if (!res.ok) {
    throw Error(`Request failed with status ${res.status}`);
  }
};

const mapToJson = map => JSON.stringify(Object.fromEntries(map));

const objectToMap = obj => new Map(Object.entries(obj));

const Spinner = () => (
  <div id="spinner">
    <aui-spinner size="large"></aui-spinner>
    <p>Loading...</p>
  </div>
);
