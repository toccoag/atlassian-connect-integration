const artifactType = new Map([
  ["Artefact_action", "Action Artifact"],
  ["Artefact_batchjob", "Batchjob Artifact"],
  ["Artefact_directive", "Directive Artifact "],
  ["Artefact_flow", "Flow Artifact"],
  ["Artefact_listener", "Listener Artifact"],
  ["Artefact_report", "Report Artifact"],
  ["Artefact_validator", "Validator Artifact"],
  ["Artefact", "Artifact"]
]);

const ArtifactSelect = ({type, artifacts, selectedArtifacts, disabled, onChange}) => {
  const select = React.useRef()

  const changeListener = ({val}) => {
    onChange(type, val)
  };

  React.useEffect(() => {
    AJS.$(select.current).auiSelect2();
    AJS.$(select.current).on("change.select2", changeListener);
  }, []);

  return <select id={`artifact-${type}-select`} multiple disabled={disabled} ref={select}>
    {artifacts.map(artifact =>
        <option key={artifact.key} value={artifact.key} selected={selectedArtifacts.includes(artifact.key)}>
          {artifact.nr} {artifact.label} ({artifact.moduleTypeLabel} / {artifact.moduleLabel})</option>
    )}
  </select>
};

const SaveButton = ({disabled, onClick}) => {
  const button = React.useRef()

  const clickListener = () => {
    onClick()
  };

  React.useEffect(() => {
    AJS.$(button.current).on("click", function () {
      if (!this.isBusy()) {
        this.busy();
        clickListener();
      }
    });
  }, []);

  React.useEffect(() => {
    AJS.toInit(function () {
      AJS.$(button.current).each(function () {
        if (disabled && this.isBusy()) {
          this.idle();
        }
      });
    });
  }, [disabled]);

  return <button className="aui-button aui-button-primary"
                 id="button-save"
                 disabled={disabled}
                 ref={button}>Save</button>
}

const App = () => {
  const [artifacts, setArtifacts] = React.useState(new Map([]));
  const [selectedArtifacts, setSelectedArtifacts] = React.useState(new Map([]));
  const [saveDisabled, setSaveDisabled] = React.useState(true);
  const [formDisabled, setFormDisabled] = React.useState(false);

  const loadData = () => {
    getData(selectedArtifactsUrl).then(result => {
      setSelectedArtifacts(objectToMap(result['artifacts']));
    })

    for (let type of artifactType.keys()) {
      getData(artifactsUrl(type)).then(result => {
        setArtifacts(artifacts => new Map(artifacts.set(type, result)));
      })
    }
  }

  React.useEffect(() => {
    loadData();
  }, []);

  const onSelectedArtifactsChange = (type, artifacts) => {
    setSelectedArtifacts(selectedArtifacts => selectedArtifacts.set(type, artifacts))
    setSaveDisabled(false)
  };

  const onClickSave = () => {
    setFormDisabled(true);
    putData(updateArtifactsUrl, mapToJson(selectedArtifacts)).then(() => {
      setSaveDisabled(true);
      setFormDisabled(false);
    });
  };

  if (artifacts.size < artifactType.size || selectedArtifacts.size === 0) {
    return <Spinner/>
  }

  return <div>
    <SaveButton disabled={saveDisabled} onClick={onClickSave}/>
    <form className="aui" style={formDisabled ? {opacity: 0.3} : {}}>{
      Array.from(artifactType.keys()).map(key => <div className="artifact">
        <b>{artifactType.get(key)}</b>
        <ArtifactSelect type={key}
                        artifacts={artifacts.get(key)}
                        selectedArtifacts={selectedArtifacts.get(key)}
                        disabled={formDisabled}
                        onChange={onSelectedArtifactsChange}/>
      </div>)
    }</form>
  </div>
};

ReactDOM.render(
  <App/>,
  document.getElementById('artifact-app')
);
