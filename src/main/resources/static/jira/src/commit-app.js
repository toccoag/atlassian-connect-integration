const InstallationSelect = ({installations, onChange}) => {
  const changeListener = ({val}) => {
    onChange(val)
  };

  React.useEffect(() => {
    AJS.$("#installation-select").auiSelect2();
    AJS.$("#installation-select").on("change.select2", changeListener);
  });

  return (
      <div id="installation-select-wrapper">
        <form className="aui">
          <select id="installation-select" data-placeholder="Select an installation">
            <option></option>
            {installations.map((installation, idx) =>
                <option key={idx} value={installation.instance}>{installation.label} ({installation.instance})</option>
            )}
          </select>
        </form>
      </div>
  )
};

const repos = {
  BACKEND: 'BACKEND',
  CLIENT: 'CLIENT'
}

const states = {
  NONE: null,
  LOADING: <aui-spinner size="small"></aui-spinner>,
  INSTALLED: <span className="aui-icon aui-icon-small aui-iconfont-check">Installed</span>,
  NOT: <span className="aui-icon aui-icon-small aui-iconfont-cross">Not Installed</span>,
  ERROR: <span className="aui-icon aui-icon-small aui-iconfont-error">Unable to check</span>
};

const CommitStatus = ({installation, commitId, repo}) => {
  const [status, setStatus] = React.useState(states.NONE);

  async function loadStatus() {
    if (installation !== null && repo === repos.BACKEND) {
      setStatus(states.LOADING);
      const checkUrl = `${host}/jira/installation/${installation}/commit/${commitId}`;
      try {
        const data = await getData(checkUrl);
        setStatus(data.installed ? states.INSTALLED : states.NOT)
      } catch (e) {
        setStatus(states.ERROR)
      }
    }
  }

  React.useEffect(() => {
    loadStatus();
  }, [installation]);

  return status
};


const DateTime = ({value}) => {
  const options = {year: 'numeric', month: 'numeric', day: 'numeric', hour: 'numeric', minute: 'numeric'}
  return new Intl.DateTimeFormat('de-CH', options).format(value)
}

const replaceLineBreaks = string => string.replace(/\n/g, '<br/>')

const replaceUrl = string => {
    const urlRegex = /(http:\/\/|https:\/\/)((\w|=|\?|\.|\+|\/|&|-)+)/g;
    return string.replace(urlRegex, "<a href='$1$2' target='_blank'>$1$2</a>");
}

const CommitList = ({commits, installation}) => (
    <table className="aui">
      <thead>
      <tr>
        <th id="nr">#</th>
        <th id="author">Author</th>
        <th id="commitMessage">Commit-Message</th>
        <th id="Installed">Installed</th>
      </tr>
      </thead>
      <tbody>
      {commits.map(({author, commitId, commitTimestamp, commitMessage, repo, installed}, idx) => (
              <tr key={idx}>
                <td headers="nr">{idx + 1}</td>
                <td headers="author">{author} <br/> <DateTime value={new Date(commitTimestamp)}/></td>
                <td headers="commitMessage">
                  <div id={'container-' + idx}>
                    <div id={'reveal-text-content-' + idx} className="aui-expander-content">
                      <div dangerouslySetInnerHTML={{__html: replaceLineBreaks(replaceUrl(commitMessage))}}/>
                      <div>
                        Commit-Id: {
                            repo === repos.CLIENT
                            ? <a href={`https://gitlab.com/toccoag/tocco-client/-/commit/${commitId}`}>{commitId}</a>
                            : commitId
                        }
                      </div>
                      <a id={'reveal-text-trigger-' + idx} data-replace-text="Show less"
                         data-replace-selector=".reveal-text-trigger-text"
                         className="aui-expander-trigger aui-expander-reveal-text" aria-controls={'reveal-text-content-' + idx}>
                        <span className="reveal-text-trigger-text">Show more</span>
                      </a>
                    </div>
                  </div>
                </td>
                <td headers="installed">
                    {
                        installed === true || installed === false ? (
                            installed ? states.INSTALLED : states.NOT
                        ) : <CommitStatus commitId={commitId} installation={installation} repo={repo}/>
                    }
                </td>
              </tr>
          )
      )}
      </tbody>
    </table>
);

const App = () => {
  const [commits, setCommits] = React.useState(null);
  const [installations, setInstallations] = React.useState(null);
  const [selectedInstallations, setSelectedInstallations] = React.useState(null);
  const [isLoadingClientCommits, setIsLoadingClientCommits] = React.useState(false);

  function loadData() {
    getData(installationUrl).then(installations => {
      setInstallations(installations)
    });

    getData(commitUrl).then(commits => {
      setCommits(commits)
    });
  }

  function loadClientCommits() {
    if (selectedInstallations) {
      const nonClientCommits = commits.filter(c => c.repo !== repos.CLIENT);
      setCommits([
        ...nonClientCommits
      ])
      setIsLoadingClientCommits(true)
      getData(`${host}/jira/installation/${selectedInstallations}/client/issue/${issueTag}`).then(newCommits => {
        setCommits([
          ...nonClientCommits,
          ...newCommits
        ])
      setIsLoadingClientCommits(false)
    });
    }
  }

  React.useEffect(() => {
    loadData();
  }, []);

  React.useEffect(() => {
    loadClientCommits();
  }, [selectedInstallations]);

  const onInstallationChange = (installation) => {
    setSelectedInstallations(installation)
  };


  if (commits === null || installations === null) {
    return <Spinner/>
  }

  return (
      <div>
        <InstallationSelect installations={installations} onChange={onInstallationChange}/>
        <br/>
        <CommitList commits={commits} installation={selectedInstallations}/>
        {isLoadingClientCommits ? <Spinner/> : null}
      </div>
  )
};

ReactDOM.render(
    <App/>,
    document.getElementById('commit-app')
);
