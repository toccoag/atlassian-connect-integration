package ch.tocco.nice2.atlassian;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CommitListViewController {

    @RequestMapping(value = "/jira/commit-view/{key}", method = RequestMethod.GET)
    public ModelAndView listCommits(@PathVariable String key) {
        ModelAndView model = new ModelAndView();
        model.setViewName("commit-list");
        model.addObject("key", key);

        return model;
    }
}
