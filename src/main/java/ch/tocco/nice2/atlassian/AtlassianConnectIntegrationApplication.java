package ch.tocco.nice2.atlassian;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

import static org.springframework.security.config.Customizer.withDefaults;

@SpringBootApplication
public class AtlassianConnectIntegrationApplication {

    public static void main(String[] args) {
        SpringApplication.run(AtlassianConnectIntegrationApplication.class, args);
    }

    /**
     * Spring Security by default adds the 'X-Frame-Options: Deny' header.
     * This needs to be disabled for our views, as they're rendered in an iframe.
     */
    @Configuration
    @EnableWebSecurity
    public static class SecurityConfiguration {

        @Bean
        public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
            return http.authorizeHttpRequests((authorize) -> authorize.requestMatchers("/jira/commit-view/**", "/jira/artifact-view/**").authenticated())
                    .httpBasic(withDefaults())
                    .csrf(AbstractHttpConfigurer::disable)
                    .build();
        }

        @Bean
        public BCryptPasswordEncoder encoder() {
            return new BCryptPasswordEncoder();
        }
    }
}
