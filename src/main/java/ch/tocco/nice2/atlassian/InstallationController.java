package ch.tocco.nice2.atlassian;

import java.util.List;

import com.atlassian.connect.spring.ContextJwt;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import ch.tocco.nice2.atlassian.beans.CommitInstalled;
import ch.tocco.nice2.atlassian.beans.CommitsInstalled;
import ch.tocco.nice2.atlassian.rest.NotFoundException;

@RestController
@RequestMapping(path = "/jira/installation")
public class InstallationController {

    @Value("${commit-service.baseUrl}")
    private String url;

    private final RestTemplate restTemplate;

    public InstallationController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @ContextJwt
    @GetMapping
    public List<Installation> listInstallations() {
        Installations installations = restTemplate.getForObject(url + "/installation", Installations.class);
        if (installations != null) {
            return installations.getData();
        } else {
            throw new NotFoundException("No installations found");
        }
    }

    @ContextJwt
    @GetMapping(value = "/{instance}/commit/{hash}")
    public IsCommitInstalledBean isInstalled(@PathVariable String instance, @PathVariable String hash) {
        return restTemplate.getForObject(url + "/installation/" + instance + "/commit/" + hash, IsCommitInstalledBean.class);
    }

    @ContextJwt
    @GetMapping(value = "/{instance}/client/issue/{key}")
    public List<CommitInstalled> listClientCommits(@PathVariable String instance, @PathVariable String key) {
        CommitsInstalled commitList = restTemplate.getForObject(url + "/installation/" + instance + "/client/issue/" + key, CommitsInstalled.class);
        if (commitList != null) {
            return commitList.getCommits();
        } else {
            throw new NotFoundException("Issue " + key + "not found");
        }
    }

    @SuppressWarnings("unused")
    public static class Installations {

        private List<Installation> data;

        private List<Installation> getData() {
            return data;
        }

        public void setData(List<Installation> data) {
            this.data = data;
        }
    }

    @SuppressWarnings("unused")
    public static class Installation {

        private String instance;
        private String label;

        public String getInstance() {
            return instance;
        }

        public void setInstance(String instance) {
            this.instance = instance;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }
    }

    @SuppressWarnings("unused")
    public static class IsCommitInstalledBean {

        private String installation;
        private String commitHash;
        private boolean isInstalled;

        public String getInstallation() {
            return installation;
        }

        public void setInstallation(String installation) {
            this.installation = installation;
        }

        public String getCommitHash() {
            return commitHash;
        }

        public void setCommitHash(String commitHash) {
            this.commitHash = commitHash;
        }

        public boolean isInstalled() {
            return isInstalled;
        }

        public void setInstalled(boolean installed) {
            isInstalled = installed;
        }
    }
}
