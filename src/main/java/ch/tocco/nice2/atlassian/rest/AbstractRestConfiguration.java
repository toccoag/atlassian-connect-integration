package ch.tocco.nice2.atlassian.rest;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

public class AbstractRestConfiguration {

    public RestTemplate createRestTemplate(String username, String password) {
        RestTemplate restTemplate = new RestTemplateBuilder()
            .basicAuthentication(username, password)
            .build();

        DefaultUriBuilderFactory uriBuilderFactory = new DefaultUriBuilderFactory();
        //already encoded by the UriComponentsBuilder
        uriBuilderFactory.setEncodingMode(DefaultUriBuilderFactory.EncodingMode.NONE);

        restTemplate.setUriTemplateHandler(uriBuilderFactory);
        restTemplate.setErrorHandler(new ErrorHandler());

        return restTemplate;
    }

    private static class ErrorHandler extends DefaultResponseErrorHandler {

        @Override
        public void handleError(ClientHttpResponse response) {
            try {
                super.handleError(response);
            } catch (RestClientResponseException e) {
                String msg = String.format("REST API call failed with code %s and message: %s", e.getRawStatusCode(), e.getResponseBodyAsString());
                throw new RestConnectionException(msg, e);
            } catch (Exception e) {
                throw new RestConnectionException("REST API call failed", e);
            }
        }
    }
}
