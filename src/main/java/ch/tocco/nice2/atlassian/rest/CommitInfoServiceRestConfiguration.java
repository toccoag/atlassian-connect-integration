package ch.tocco.nice2.atlassian.rest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class CommitInfoServiceRestConfiguration extends AbstractRestConfiguration {

    @Value("${commit-service.user}")
    private String userName;
    @Value("${commit-service.password}")
    private String password;

    @Bean
    public RestTemplate createRestTemplate() {
        return createRestTemplate(userName, password);
    }
}
