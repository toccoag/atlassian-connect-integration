package ch.tocco.nice2.atlassian.rest;

public class RestConnectionException extends RuntimeException {

    public RestConnectionException(String message, Throwable cause) {
        super(message, cause);
    }
}
