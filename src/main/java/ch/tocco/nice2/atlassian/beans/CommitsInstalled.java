package ch.tocco.nice2.atlassian.beans;

import java.util.ArrayList;
import java.util.List;

public class CommitsInstalled {

    private List<CommitInstalled> commits = new ArrayList<>();

    public List<CommitInstalled> getCommits() {
        return commits;
    }

    public void setCommits(List<CommitInstalled> commits) {
        this.commits = commits;
    }
}
