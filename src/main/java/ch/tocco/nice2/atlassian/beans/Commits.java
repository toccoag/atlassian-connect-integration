package ch.tocco.nice2.atlassian.beans;

import java.util.ArrayList;
import java.util.List;

public class Commits {

    private List<Commit> commits = new ArrayList<>();

    public List<Commit> getCommits() {
        return commits;
    }

    public void setCommits(List<Commit> commits) {
        this.commits = commits;
    }
}
