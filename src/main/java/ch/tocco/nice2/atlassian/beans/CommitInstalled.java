package ch.tocco.nice2.atlassian.beans;

public class CommitInstalled extends Commit {

    private boolean installed;

    public boolean isInstalled() {
        return installed;
    }

    public void setInstalled(boolean installed) {
        this.installed = installed;
    }
}