package ch.tocco.nice2.atlassian;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.atlassian.connect.spring.ContextJwt;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import ch.tocco.nice2.atlassian.rest.NotFoundException;

@RestController
@RequestMapping(path = "/jira/artifact")
public class ArtifactController {

    @Value("${commit-service.baseUrl}")
    private String url;

    private final RestTemplate restTemplate;

    public ArtifactController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @ContextJwt
    @GetMapping(value = "/{modelName}")
    public List<Artifact> getAllArtifacts(@PathVariable String modelName) {
        ArtifactList artifactList = restTemplate.getForObject(url + "/artifact/" + modelName, ArtifactList.class);
        if (artifactList != null) {
            return artifactList.getArtifacts();
        } else {
            throw new NotFoundException("Artifact " + modelName + "not found");
        }
    }

    @ContextJwt
    @GetMapping("/issue/{issueKey}")
    public ArtifactsBean<String> getArtifacts(@PathVariable String issueKey) {
        ArtifactMapBean artifactMapBean = restTemplate.getForObject(url + "/artifact/issue/" + issueKey, ArtifactMapBean.class);
        if (artifactMapBean != null) {
            Map<String, List<String>> map = artifactMapBean.getArtifacts()
                    .entrySet()
                    .stream()
                    .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue()
                            .stream()
                            .map(Artifact::getKey)
                            .collect(Collectors.toList())));
            ArtifactsBean<String> resultBean = new ArtifactsBean<>();
            resultBean.setArtifacts(map);
            return resultBean;
        } else {
            throw new NotFoundException("Issue " + issueKey + "not found");
        }
    }

    @ContextJwt
    @PutMapping("/issue/{issueKey}")
    public void putArtifacts(@PathVariable String issueKey, @RequestBody Map<String, List<String>> artifacts) {
        ArtifactsUpdateBean bean = new ArtifactsUpdateBean();
        bean.setArtifacts(artifacts);
        restTemplate.put(url + "/artifact/issue/" + issueKey, bean);
    }

    @SuppressWarnings("unused")
    public static class ArtifactList {

        private List<Artifact> artifacts = List.of();

        public List<Artifact> getArtifacts() {
            return artifacts;
        }

        @JsonProperty("data")
        public void setArtifacts(List<Artifact> data) {
            this.artifacts = data;
        }
    }

    @SuppressWarnings("unused")
    public static class Artifact {
        private String key;
        private String nr;
        private String label;
        private String moduleLabel;
        private String moduleTypeLabel;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getNr() {
            return nr;
        }

        public void setNr(String nr) {
            this.nr = nr;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getModuleLabel() {
            return moduleLabel;
        }

        public void setModuleLabel(String moduleLabel) {
            this.moduleLabel = moduleLabel;
        }

        public String getModuleTypeLabel() {
            return moduleTypeLabel;
        }

        public void setModuleTypeLabel(String moduleTypeLabel) {
            this.moduleTypeLabel = moduleTypeLabel;
        }
    }

    @SuppressWarnings("unused")
    public static class ArtifactsBean<T> {
        private Map<String, List<T>> artifacts;

        public Map<String, List<T>> getArtifacts() {
            return artifacts;
        }

        public void setArtifacts(Map<String, List<T>> artifacts) {
            this.artifacts = artifacts;
        }
    }

    public static class ArtifactMapBean extends ArtifactsBean<Artifact> {

    }

    @SuppressWarnings("unused")
    public static class ArtifactsUpdateBean {
        private Map<String, List<String>> artifacts;

        public Map<String, List<String>> getArtifacts() {
            return artifacts;
        }

        public void setArtifacts(Map<String, List<String>> artifacts) {
            this.artifacts = artifacts;
        }
    }
}
