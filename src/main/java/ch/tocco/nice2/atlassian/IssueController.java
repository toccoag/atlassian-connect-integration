package ch.tocco.nice2.atlassian;

import java.util.List;

import com.atlassian.connect.spring.ContextJwt;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import ch.tocco.nice2.atlassian.beans.Commit;
import ch.tocco.nice2.atlassian.beans.Commits;
import ch.tocco.nice2.atlassian.rest.NotFoundException;

@RestController
@RequestMapping(path = "/jira/issue")
public class IssueController {

    @Value("${commit-service.baseUrl}")
    private String url;

    private final RestTemplate restTemplate;

    public IssueController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @ContextJwt
    @GetMapping(value = "/{issue}/commit")
    public List<Commit> listCommits(@PathVariable String issue) {
        Commits commitList = restTemplate.getForObject(url + "/issue/" + issue + "/commit", Commits.class);
        if (commitList != null) {
            return commitList.getCommits();
        } else {
            throw new NotFoundException("Issue " + issue + "not found");
        }
    }
}
