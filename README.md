# atlassian-connect-integration

## Getting started

**Build:**
```
./gradlew build
```

**Create empty PostgreSQL database and add local config file:**

Config file loaction: `src/main/resources/application-dev.properties`:

Required properties in new file:
```
spring.datasource.url=jdbc:postgresql://localhost:5432/jira_addon_local
spring.datasource.username=****
spring.datasource.password=****

commit-service.baseUrl=https://commit-info-service.tocco.ch
commit-service.user=****
commit-service.password=****

```

**Disable JWT authentication locally (don't commit accidentally):**
- Comment out ``'Authorization': `JWT ${tokenTag}` `` in `helpers.js`
- Annotate REST controllers with `@IgnoreJwt`


**Start application:**

Run `ch.tocco.nice2.atlassian.AtlassianConnectIntegrationApplication` with 
VM option `-Dspring.profiles.active=dev`

**Open view in browser:**

http://localhost:8080/jira/commit-view/**${Jira Issue Key}**
(e.g. http://localhost:8080/jira/commit-view/TOCDEV-429)
